class Main {
  public static void main(String[] args) {
    Kello kello = new Kello();
    kello.setAika(1415);
    Kello clone = (Kello) kello.clone();
    clone.setAika(1515);
    System.out.println(kello.getAika());
    System.out.println(clone.getAika());

  }
}
